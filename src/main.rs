extern crate piston;
extern crate piston_window;
extern crate conv;
extern crate rand;

use rand::Rng;
use conv::ConvUtil;
use piston_window::{ Rectangle, PistonWindow, rectangle, Events, WindowSettings, Input, Button };

const GRID_SIZE:   usize = 100;
const CELL_SIZE:   f64   = 10.0;
const CELL_COLOR: [f32; 4] = [0.0, 0.0, 0.0, 1.0];

type Grid = [[bool; GRID_SIZE]; GRID_SIZE];

fn draw(grid: &Grid, frame: Events, window: &mut PistonWindow) {
    window.draw_2d(&frame, |c, g| {
        // clear([1.0; 4], g);
        for (y, row) in grid.iter().enumerate() {
            for (x, value) in row.iter().enumerate() {
                let rect_bounds = [
                    x.value_as::<f64>().unwrap() * CELL_SIZE,
                    y.value_as::<f64>().unwrap() * CELL_SIZE,
                    CELL_SIZE,
                    CELL_SIZE,
                ];
                
                if *value {
                    rectangle(CELL_COLOR, rect_bounds, c.transform, g);
                } else {
                    rectangle([1.0; 4], rect_bounds, c.transform, g);
                }
            }
        }
    });
}

fn count_neighbours(x: usize, y: usize, grid: &Grid) -> u8 {
    let mut res: u8 = 0;
    let up    = if y == 0 { GRID_SIZE } else { y - 1 };
    let left  = if x == 0 { GRID_SIZE } else { x - 1 };
    let down  = if y == GRID_SIZE - 1 { 0 } else { y + 1 };
    let right = if x == GRID_SIZE - 1 { 0 } else { x + 1 };

    if grid.get(up).map_or(false,   |row| *row.get(left).unwrap_or(&false)) { res += 1; }
    if grid.get(up).map_or(false,   |row| *row.get(x).unwrap_or(&false)) { res += 1; }
    if grid.get(up).map_or(false,   |row| *row.get(right).unwrap_or(&false)) { res += 1; }
    if grid.get(down).map_or(false, |row| *row.get(left).unwrap_or(&false)) { res += 1; }
    if grid.get(down).map_or(false, |row| *row.get(x).unwrap_or(&false)) { res += 1; }
    if grid.get(down).map_or(false, |row| *row.get(right).unwrap_or(&false)) { res += 1; }
    if grid.get(y).map_or(false,    |row| *row.get(left).unwrap_or(&false)) { res += 1; }
    if grid.get(y).map_or(false,    |row| *row.get(right).unwrap_or(&false)) { res += 1; }

    res
}

fn update(grid: &Grid) -> Grid {
    let mut new_grid: Grid = [[false; GRID_SIZE]; GRID_SIZE];

    for (y, row) in new_grid.iter_mut().enumerate() {
        for (x, cell) in row.iter_mut().enumerate() {
            *cell = *grid.get(y).unwrap().get(x).unwrap();
            let count = count_neighbours(x, y, grid);

            if count < 2 {
                *cell = false;
            } else if count > 3 {
                *cell = false;
            } else if count == 3 {
                *cell = true;
            }
        }
    }

    new_grid
}

fn random_grid() -> Grid {
    let mut rng = rand::thread_rng();
    let mut grid = [[false; GRID_SIZE]; GRID_SIZE];

    for row in grid.iter_mut() {
        for e in row.iter_mut() {
            *e = rng.gen();
        }
    }

    grid
}

fn main() {
    let mut grid = random_grid();

    let mut window: PistonWindow = WindowSettings::new("Hello Piston!", [1000, 1000])
        .exit_on_esc(true)
        .build()
        .unwrap();

    while let Some(event) = window.next() {
        match event {
            Events::Render(_) => draw(&grid, event, &mut window),
            Events::Update(_) => grid = update(&grid),
            Events::Input(Input::Press(Button::Keyboard(key))) => {
                match key {
                    Space => grid = random_grid()
                }
            },
            _ => (),
        }
    }
}
